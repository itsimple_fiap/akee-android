package com.istimple.akee.util

const val BASE_URL: String = "http://www.mocky.io"
const val EVENTO_ID: String = "EVENTO_ID"
const val LOGIN: String = "LOGIN"
const val TAG_EVENTO_FRAGMENT = "EventoList"
const val TAG_EVENTO_FAVORITOS_FRAGMENT = "EventoFavoritosList"
