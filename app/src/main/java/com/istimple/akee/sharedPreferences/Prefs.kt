package com.istimple.akee.sharedPreferences

import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context) {
    val PREFS_FILENAME = "com.akee.itsimple.akee"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)
    val USUARIO_LOGADO = "USUARIO_LOGADO"

    var usuarioLogado: Boolean
        get() = prefs.getBoolean(USUARIO_LOGADO, false)
        set(value) = prefs.edit().putBoolean(USUARIO_LOGADO, value).apply()

    fun limparPrefs() {
        prefs.edit().clear().apply()
    }
}