package com.istimple.akee.network

import com.istimple.akee.model.Evento
import com.istimple.akee.model.User
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Interface que fornece os métodos para acessar a API REST
 */
interface EventoApi {

    /**
     * Acessa a lista de eventos da API
     */
    @GET("/v2/5b821eec3400008f00ecb3ec")
    fun getEventos(): Single<List<Evento>>

    @GET("/v2/{evento}")
    fun getDetalheDoEvento(@Path("evento") idEvento: String): Single<Evento>

    @GET("/v2/5b835a043300004f0095952f{user}")
    fun getEventosFavoritos(@Path("user") idUser: String): Single<List<Evento>>

    @GET("/v2/5b849719310000ee360d23ba/{email}{senha}")
    fun realizarLogin(@Path("email") email: String, @Path("senha") senha: String): Single<User>

    @POST("/v2/5b849719310000ee360d23ba")
    fun realizarCadastro(@Body user: User): Single<User>

    @POST("/v2/5b849719310000ee360d23ba")
    fun realizarAlteracao(@Body user: User): Single<User>
}