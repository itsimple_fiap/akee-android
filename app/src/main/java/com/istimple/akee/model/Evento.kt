package com.istimple.akee.model

import com.squareup.moshi.Json

data class Evento(
        @Json(name = "id")
        var id: String? = null,

        @Json(name = "preco")
        var preco: Float? = null,

        @Json(name = "nome")
        var nome: String? = null,

        @Json(name = "descricao")
        var descricao: String? = null,

        @Json(name = "imagem")
        var imagem: String? = null
)