package com.istimple.akee.model


data class User(
        val userCode: String,
        val nome: String,
        val email: String,
        val senha: String?,
        val imagem: String?)