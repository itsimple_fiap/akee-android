package com.istimple.akee.model

import com.squareup.moshi.Json

data class Categoria(
        @Json(name = "id")
        var id: Long? = null,

        @Json(name = "descricao")
        var descricao: String? = null
)