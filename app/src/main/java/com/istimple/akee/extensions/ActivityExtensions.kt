package com.istimple.akee.extensions

import android.app.Activity
import android.content.Intent

fun Activity.callGaleryActivity(requestCode: Int){
    val intent = Intent(Intent.ACTION_GET_CONTENT)
    intent.type = "image/jpeg"
    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
    this.startActivityForResult(Intent.createChooser(intent, "Complete action using"), requestCode)
}