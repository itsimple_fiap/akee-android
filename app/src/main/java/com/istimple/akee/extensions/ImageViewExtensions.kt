package com.istimple.akee.extensions

import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Callback
import java.lang.Exception
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.graphics.drawable.BitmapDrawable
import com.itsimple.akee.R


fun ImageView.setRoundedImage(widthImg: Int, heightImg: Int, imgUri: Uri){

    Picasso.get()
            .load(imgUri)
            .placeholder(R.drawable.ic_person_flat )
            .resize(widthImg, heightImg)
            .centerCrop()
            .into(this, object : Callback{

                override fun onSuccess() {

                    val imageBitmap = (this@setRoundedImage.drawable as BitmapDrawable).bitmap
                    val imageDrawable = RoundedBitmapDrawableFactory.create(resources, imageBitmap)
                    imageDrawable.isCircular = true
                    imageDrawable.cornerRadius = Math.max(imageBitmap.width, imageBitmap.height) / 2.0f
                    this@setRoundedImage.setImageDrawable(imageDrawable)
                }

                override fun onError(e: Exception?) {
                    val e = ""
                }

            })
}