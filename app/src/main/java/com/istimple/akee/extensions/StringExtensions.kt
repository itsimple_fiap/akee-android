package com.istimple.akee.extensions

fun String.isEmail() : Boolean{
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}