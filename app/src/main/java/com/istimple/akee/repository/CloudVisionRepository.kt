package com.istimple.akee.repository

import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.vision.v1.Vision
import com.google.api.services.vision.v1.VisionRequestInitializer
import com.google.api.services.vision.v1.model.*
import io.reactivex.Observable
import java.util.*

const val CLOUD_VISION_API_KEY = "AIzaSyCbUvH4MsV57h0WfYw2CylH4M_TC0taUlM"

class CloudVisionRepository: CloudVisionRepositoryContract {

    override fun callCloudVision(annotateImageRequests: ArrayList<AnnotateImageRequest>,
                                 api: String): Observable<String> {

        return Observable.create<String> {
            val httpTransport = AndroidHttp.newCompatibleTransport()
            val jsonFactory = GsonFactory.getDefaultInstance()

            val requestInitializer = VisionRequestInitializer(CLOUD_VISION_API_KEY)

            val builder = Vision.Builder(httpTransport, jsonFactory, null)
            builder.setVisionRequestInitializer(requestInitializer)

            val vision = builder.build()

            val batchAnnotateImagesRequest = BatchAnnotateImagesRequest()
            batchAnnotateImagesRequest.requests = annotateImageRequests

            val annotateRequest = vision.images().annotate(batchAnnotateImagesRequest)
            annotateRequest.disableGZipContent = true

            val response = annotateRequest.execute()
            it.onNext(convertResponseToString(response, api))
        }
    }

    private fun convertResponseToString(response: BatchAnnotateImagesResponse, api: String): String {

        val imageResponses = response.responses[0]

        var entityAnnotations: List<EntityAnnotation>

        var message = "not found"
        when (api) {
            "LANDMARK_DETECTION" -> {
                imageResponses.landmarkAnnotations?.let {
                    entityAnnotations = imageResponses.landmarkAnnotations
                    message = formatAnnotation(entityAnnotations)
                }
            }
            "LOGO_DETECTION" -> {
                imageResponses.logoAnnotations?.let {
                    entityAnnotations = imageResponses.logoAnnotations
                    message = formatAnnotation(entityAnnotations)
                }
            }
            "SAFE_SEARCH_DETECTION" -> {
                imageResponses.safeSearchAnnotation?.let {
                    val annotation = imageResponses.safeSearchAnnotation
                    message = getImageAnnotation(annotation)
                }
            }
            "IMAGE_PROPERTIES" -> {
                imageResponses.imagePropertiesAnnotation?.let {
                    val imageProperties = imageResponses.imagePropertiesAnnotation
                    message = getImageProperty(imageProperties)
                }
            }
            "LABEL_DETECTION" -> {
                imageResponses.labelAnnotations?.let {
                    entityAnnotations = imageResponses.labelAnnotations
                    message = formatAnnotation(entityAnnotations)
                }
            }
        }
        return message
    }

    private fun formatAnnotation(entityAnnotation: List<EntityAnnotation>?): String {
        var message = ""

        if (entityAnnotation != null) {
            for (entity in entityAnnotation) {
                message = message + "    " + entity.description + " " + entity.score
                message += "\n"
            }
        } else {
            message = "Nothing Found"
        }
        return message
    }



    private fun getImageAnnotation(annotation: SafeSearchAnnotation): String {
        return String.format("adult: %s\nmedical: %s\nspoofed: %s\nviolence: %s\n",
                annotation.adult,
                annotation.medical,
                annotation.spoof,
                annotation.violence)
    }

    private fun getImageProperty(imageProperties: ImageProperties): String {
        var message = ""
        val colors = imageProperties.dominantColors
        for (color in colors.colors) {
            message = message + "" + color.pixelFraction + " - " + color.color.red + " - " + color.color.green + " - " + color.color.blue
            message += "\n"
        }
        return message
    }


}