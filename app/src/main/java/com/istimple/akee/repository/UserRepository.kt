package com.istimple.akee.repository

import com.istimple.akee.model.User
import com.istimple.akee.network.EventoApi
import io.reactivex.Single

class UserRepository(private var eventoApi: EventoApi): UserRepositoryContract {

    override fun realizarAlteracao(user: User): Single<User> {
        return eventoApi.realizarAlteracao(user)
    }

    override fun realizarCadastro(user: User): Single<User> {
        return eventoApi.realizarCadastro(user)
    }

    override fun realizarLogin(email: String, senha: String): Single<User> {
        return eventoApi.realizarLogin(email, senha)
    }

}