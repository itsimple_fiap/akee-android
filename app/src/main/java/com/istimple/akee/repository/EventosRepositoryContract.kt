package com.istimple.akee.repository

import com.istimple.akee.model.Evento
import io.reactivex.Single

interface EventosRepositoryContract {
    fun getEventos(): Single<List<Evento>>
    fun getDetalheDoEvento(idEvento: String): Single<Evento>
    fun getEventosFavoritos(idUser: String): Single<List<Evento>>
}

