package com.istimple.akee.repository

import com.istimple.akee.model.User
import io.reactivex.Single

interface UserRepositoryContract {

    fun realizarLogin(email: String, senha: String): Single<User>

    fun realizarCadastro(user: User): Single<User>

    fun realizarAlteracao(user: User): Single<User>
}