package com.istimple.akee.repository

import com.google.api.services.vision.v1.model.AnnotateImageRequest
import io.reactivex.Observable
import java.util.*

interface CloudVisionRepositoryContract {
    fun callCloudVision(annotateImageRequests: ArrayList<AnnotateImageRequest>, api: String): Observable<String>
}