package com.istimple.akee.repository

import com.istimple.akee.model.Evento
import com.istimple.akee.network.EventoApi
import io.reactivex.Single

class EventosRepository(private var eventoApi: EventoApi): EventosRepositoryContract {

    override fun getEventosFavoritos(idUser: String): Single<List<Evento>> {
        return eventoApi.getEventosFavoritos(idUser)
    }

    override fun getEventos(): Single<List<Evento>> {
        return eventoApi.getEventos()
    }

    override fun getDetalheDoEvento(idEvento: String): Single<Evento> {
        return eventoApi.getDetalheDoEvento(idEvento)
    }
}