package com.istimple.akee.view.login

import com.istimple.akee.util.mvp.BasePresenter
import com.istimple.akee.util.mvp.BaseView

interface LoginContract {
    interface View: BaseView<Presenter> {

        fun showLoading()

        fun hideLoading()

        fun loginComSucesso()

        fun showError(msg: String)
    }

    interface Presenter: BasePresenter<View> {
        fun realizarLogin(email: String, senha: String)
    }
}