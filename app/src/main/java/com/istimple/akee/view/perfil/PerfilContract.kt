package com.istimple.akee.view.perfil

import com.istimple.akee.model.User
import com.istimple.akee.util.mvp.BasePresenter
import com.istimple.akee.util.mvp.BaseView

interface PerfilContract {
    interface View: BaseView<Presenter> {

        fun showLoading()

        fun hideLoading()

        fun cadastradoComSucesso()

        fun alteradoComSucesso()

        fun showError(msg: String)
    }

    interface Presenter: BasePresenter<View> {
        var user: User

        fun realizarCadastro(user: User)

        fun realizarAlteracao(user: User)
    }
}