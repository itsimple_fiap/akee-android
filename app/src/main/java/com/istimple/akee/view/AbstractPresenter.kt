package com.istimple.akee.view

import android.support.annotation.CallSuper
import com.istimple.akee.util.mvp.BasePresenter
import com.istimple.akee.util.mvp.BaseView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class AbstractPresenter<V : BaseView<P>, out P : BasePresenter<V>> : BasePresenter<V> {

    override lateinit var view: V

    val disposables = CompositeDisposable()

    fun execute(block: () -> Disposable) {
        disposables.add(block())
    }

    /**
     * Esse método pode ser chamado quando o Presenter da tela é destruido
     */
    @CallSuper
    override fun stop() {
        disposables.clear()
    }
}