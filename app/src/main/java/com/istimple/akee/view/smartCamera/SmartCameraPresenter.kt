package com.istimple.akee.view.smartCamera

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.google.api.services.vision.v1.model.AnnotateImageRequest
import com.google.api.services.vision.v1.model.Feature
import com.google.api.services.vision.v1.model.Image
import com.istimple.akee.repository.CloudVisionRepositoryContract
import com.istimple.akee.view.AbstractPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.ByteArrayOutputStream
import java.util.*

class SmartCameraPresenter(override var view: SmartCameraContract.View,
                           private val cloudVisionRepository: CloudVisionRepositoryContract) :
        AbstractPresenter<SmartCameraContract.View, SmartCameraContract.Presenter>(),
        SmartCameraContract.Presenter {

    override var visionAPI = listOf("LANDMARK_DETECTION", "LOGO_DETECTION", "SAFE_SEARCH_DETECTION", "IMAGE_PROPERTIES", "LABEL_DETECTION")
    override var api = visionAPI[0]


    override fun callCloudVision(bitmap: Bitmap, feature: Feature) {
        val featureList = ArrayList<Feature>()
        featureList.add(feature)

        val annotateImageRequests = ArrayList<AnnotateImageRequest>()

        val annotateImageReq = AnnotateImageRequest()
        annotateImageReq.features = featureList
        annotateImageReq.image = getImageEncodeImage(bitmap)
        annotateImageRequests.add(annotateImageReq)

        execute {
            cloudVisionRepository
                    .callCloudVision(annotateImageRequests, api)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        view.startRequest()
                    }.doOnError {
                        view.showError("Not found")
                    }.subscribe {
                        view.requestSuccess(it)
                        view.closeLoading()
                    }
        }
    }

    override fun makeRequest(activity: Activity, permission: String) {
        ActivityCompat.requestPermissions(activity, arrayOf(permission), SmartCameraActivity.RECORD_REQUEST_CODE)
    }

    override fun checkPermission(context: Context, permission: String): Int {
        return ContextCompat.checkSelfPermission(context, permission)
    }

    private fun getImageEncodeImage(bitmap: Bitmap): Image {
        val base64EncodedImage = Image()
        // Convert the bitmap to a JPEG
        // Just in case it's a format that Android understands but Cloud Vision
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream)
        val imageBytes = byteArrayOutputStream.toByteArray()

        // Base64 encode the JPEG
        base64EncodedImage.encodeContent(imageBytes)
        return base64EncodedImage
    }
}