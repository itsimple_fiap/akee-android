package com.istimple.akee.view.detalheevento

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.istimple.akee.di.Parametros.DETALHE_EVENTO_VIEW
import com.istimple.akee.util.EVENTO_ID
import com.itsimple.akee.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detalhe_do_evento.*
import org.koin.android.ext.android.inject


class DetalheDoEventoActivity : AppCompatActivity(), DetalheDoEventoContract.View {

    override val presenter: DetalheDoEventoContract.Presenter by inject{ mapOf(DETALHE_EVENTO_VIEW to this) }
    private val idEvento by lazy { intent.getStringExtra(EVENTO_ID) }

    companion object {
        fun newIntent(ctx: Context, eventoId: String = "") = Intent(ctx, DetalheDoEventoActivity::class.java)
                .putExtra(EVENTO_ID, eventoId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhe_do_evento)

        title = getString(R.string.title_activity_detalhe_do_evento)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        presenter.getDetalheDoEvento(idEvento)

        buttonEuVou.setOnClickListener {
            Toast.makeText(this, getString(R.string.parabens), Toast.LENGTH_SHORT).show()
        }
    }

    override fun showLoading() {
        groupDetalhe.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun updateView() {
        textViewNomeEvento.text = presenter.evento.nome
        textViewDescricaoEvento.text = presenter.evento.descricao
        textViewPrecoEvento.text = if(presenter.evento.preco.toString() == "0.0") getString(R.string.fratis) else presenter.evento.preco.toString()
        Picasso.get()
                .load(presenter.evento.imagem)
                .into(imageViewEvento)

        groupDetalhe.visibility = View.VISIBLE
    }

    override fun showError(msg: String) {
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onDestroy() {
        presenter.stop()
        super.onDestroy()
    }
}
