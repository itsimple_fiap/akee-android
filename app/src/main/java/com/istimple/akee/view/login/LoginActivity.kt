package com.istimple.akee.view.login

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.istimple.akee.di.Parametros.LOGIN_VIEW
import com.istimple.akee.extensions.isEmail
import com.istimple.akee.sharedPreferences.Prefs
import com.istimple.akee.view.main.MainActivity
import com.istimple.akee.view.perfil.PerfilActivity
import com.itsimple.akee.R
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity(), LoginContract.View {

    override val presenter: LoginContract.Presenter by inject { mapOf(LOGIN_VIEW to this) }

    companion object {
        fun newIntent(ctx: Context) = Intent(ctx, LoginActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        textViewCadastrese.setOnClickListener {
            startActivity(PerfilActivity.newIntent(this, true))
        }

        buttonEntrar.setOnClickListener {
            clearErrorHint()
            if (validateFields()) {
                presenter.realizarLogin(editTextEmail.text.toString(), editTextSenha.text.toString())
            }
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun loginComSucesso() {
        Prefs(this).usuarioLogado = true
        startActivity(MainActivity.newIntent(this))
        finishAffinity()
    }

    override fun showError(msg: String) {
        Toast.makeText(this, getString(R.string.erro), Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        presenter.stop()
        super.onDestroy()
    }

    private fun clearErrorHint() {
        textInputLayoutEmail.error = null
        textInputLayoutSenha.error = null
    }

    private fun validateFields(): Boolean {

        return when {
            editTextEmail.text.toString().isEmpty()
                    || !editTextEmail.text.toString().isEmail() -> {

                textInputLayoutEmail.error = getString(R.string.email_invalido)
                false
            }
            editTextSenha.text.toString().isEmpty() -> {
                textInputLayoutSenha.error = getString(R.string.senha_invalida)
                false
            }
            else -> true
        }
    }
}
