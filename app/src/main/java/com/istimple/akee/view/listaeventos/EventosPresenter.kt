package com.istimple.akee.view.listaeventos

import com.istimple.akee.model.Evento
import com.istimple.akee.repository.EventosRepositoryContract
import com.istimple.akee.view.AbstractPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class EventosPresenter(override var view: EventosContract.View,
                       private val repository: EventosRepositoryContract) :
        AbstractPresenter<EventosContract.View, EventosContract.Presenter>(), EventosContract.Presenter {

    override var eventosList = listOf<Evento>()

    override fun getEventos() {
        execute {
            repository.getEventos()
                    .toObservable()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        view.showLoading()
                    }.doOnComplete {
                        view.hideLoading()
                    }.doOnError {
                        view.showError("")
                    }.subscribe {
                        eventosList = it
                        view.updateEventos()
                    }
        }
    }
}