package com.istimple.akee.view.eventosfavoritos

import com.istimple.akee.model.Evento
import com.istimple.akee.util.mvp.BasePresenter
import com.istimple.akee.util.mvp.BaseView

interface FavoritosContract {

    interface View: BaseView<Presenter> {

        fun showLoading()

        fun hideLoading()

        fun updateEventosFavoritos()

        fun showError(msg: String)
    }

    interface Presenter: BasePresenter<View> {
        var listEventosFavoritost: List<Evento>

        fun getEventosFavoritos(idUser: String)
    }
}