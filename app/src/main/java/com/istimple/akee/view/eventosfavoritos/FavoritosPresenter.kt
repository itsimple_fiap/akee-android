package com.istimple.akee.view.eventosfavoritos

import com.istimple.akee.model.Evento
import com.istimple.akee.repository.EventosRepositoryContract
import com.istimple.akee.view.AbstractPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FavoritosPresenter(override var view: FavoritosContract.View,
                         private val repository: EventosRepositoryContract) :
        AbstractPresenter<FavoritosContract.View, FavoritosContract.Presenter>(), FavoritosContract.Presenter {

    override var listEventosFavoritost = listOf<Evento>()

    override fun getEventosFavoritos(idUser: String) {
        execute {
            repository.getEventosFavoritos(idUser)
                    .toObservable()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        view.showLoading()
                    }.doOnComplete {
                        view.hideLoading()
                    }.doOnError {
                        view.showError("")
                    }.subscribe {
                        listEventosFavoritost = it
                        view.updateEventosFavoritos()
                    }
        }
    }

}
