package com.istimple.akee.view.eventosfavoritos

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.istimple.akee.di.Parametros.FAVORITOS_VIEW
import com.istimple.akee.view.detalheevento.DetalheDoEventoActivity
import com.itsimple.akee.R
import kotlinx.android.synthetic.main.fragment_favoritos.*
import org.koin.android.ext.android.inject

class FavoritosFragment : Fragment(), FavoritosContract.View {

    override val presenter: FavoritosContract.Presenter by inject { mapOf(FAVORITOS_VIEW to this) }
    private val eventosFavoritosAdapter = FavoritosAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_favoritos, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        presenter.getEventosFavoritos("")
        onClickEventoLista()
    }

    override fun updateEventosFavoritos() {
        eventosFavoritosAdapter.listaEventosFavoritos = presenter.listEventosFavoritost
    }

    private fun setUpRecyclerView() {
        with(recyclerViewFavoritos) {
            layoutManager = LinearLayoutManager(context)
            adapter = eventosFavoritosAdapter
        }
    }

    private fun onClickEventoLista() {
        eventosFavoritosAdapter
                .eventoClicado()
                .subscribe {
                    startActivity(DetalheDoEventoActivity.newIntent(context!!, it))
                }
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun showError(msg: String) {

    }

    override fun onDestroy() {
        presenter.stop()
        super.onDestroy()
    }



}
