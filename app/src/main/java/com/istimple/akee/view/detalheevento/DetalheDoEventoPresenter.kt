package com.istimple.akee.view.detalheevento

import com.istimple.akee.model.Evento
import com.istimple.akee.repository.EventosRepositoryContract
import com.istimple.akee.view.AbstractPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetalheDoEventoPresenter(override var view: DetalheDoEventoContract.View, val repository: EventosRepositoryContract) : AbstractPresenter<DetalheDoEventoContract.View, DetalheDoEventoContract.Presenter>(), DetalheDoEventoContract.Presenter {

    override var evento = Evento()

    override fun getDetalheDoEvento(idEvento: String) {
        execute {
            repository.getDetalheDoEvento(idEvento)
                    .toObservable()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe {
                        view.showLoading()
                    }.doOnComplete {
                        view.hideLoading()
                    }.doOnError {
                        view.showError("")
                    }.subscribe {
                        evento = it
                        view.updateView()
                    }
        }
    }

}