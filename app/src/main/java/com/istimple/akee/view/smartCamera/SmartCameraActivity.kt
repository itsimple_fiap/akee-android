package com.istimple.akee.view.smartCamera

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.api.services.vision.v1.model.Feature
import com.istimple.akee.di.Parametros
import com.itsimple.akee.R
import kotlinx.android.synthetic.main.activity_smart_camera.*
import org.koin.android.ext.android.inject

class SmartCameraActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, SmartCameraContract.View {

    override val presenter: SmartCameraContract.Presenter by inject { mapOf(Parametros.SMART_CAMERA to this) }

    private val feature: Feature = Feature()
    private var bitmap: Bitmap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_smart_camera)

        featureConfig()
        setupSpinner()

        bt_takePicture.setOnClickListener { takePictureFromCamera() }
    }

    override fun takePictureFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            bitmap = data!!.extras!!.get("data") as Bitmap
            iv_image.setImageBitmap(bitmap)
            presenter.callCloudVision(bitmap!!, feature)
        }
    }

    override fun onResume() {
        super.onResume()
        if (checkPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            bt_takePicture.visibility = View.VISIBLE
        } else {
            bt_takePicture.visibility = View.INVISIBLE
            presenter.makeRequest(this, Manifest.permission.CAMERA)
        }
    }

    override fun startRequest() {
        imageProgress.visibility = View.VISIBLE
    }

    override fun requestSuccess(result: String) {
        tv_API_data.text = result
    }

    override fun closeLoading() {
        imageProgress.visibility = View.INVISIBLE
    }

    override fun showError(error: String) {
        tv_API_data.text = error
    }

    private fun checkPermission(permission: String): Int {
        return ContextCompat.checkSelfPermission(this, permission)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun featureConfig() {
        feature.type = presenter.visionAPI[0]
        feature.maxResults = 10
    }

    private fun setupSpinner() {
        sp_visionAPI.onItemSelectedListener = this

        val dataAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, presenter.visionAPI)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        sp_visionAPI.adapter = dataAdapter
    }

    override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
        presenter.api = adapterView.getItemAtPosition(i) as String
        feature.type = presenter.api
        if (bitmap != null)
            presenter.callCloudVision(bitmap!!, feature)
    }

    companion object {
        const val RECORD_REQUEST_CODE = 101
        const val CAMERA_REQUEST_CODE = 102
        const val TAG = "MainActivity"

        fun newIntent(ctx: Context) = Intent(ctx, SmartCameraActivity::class.java)
    }
}
