package com.istimple.akee.view.listaeventos

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.istimple.akee.di.Parametros.EVENTOS_VIEW
import com.istimple.akee.view.detalheevento.DetalheDoEventoActivity
import com.itsimple.akee.R
import kotlinx.android.synthetic.main.fragment_eventos.progressBar
import kotlinx.android.synthetic.main.fragment_eventos.recyclerViewEventos
import org.koin.android.ext.android.inject

/**
 * Activity que exibe a lista de eventos
 */
class EventosFragment : Fragment(), EventosContract.View {

    override val presenter: EventosContract.Presenter by inject { mapOf(EVENTOS_VIEW to this) }
    private val eventosAdapter = EventosAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_eventos, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        presenter.getEventos()
        onClickEventoLista()
    }

    override fun updateEventos() {
        eventosAdapter.listaEventos = presenter.eventosList
    }

    private fun setUpRecyclerView() {
        with(recyclerViewEventos) {
            layoutManager = LinearLayoutManager(context)
            adapter = eventosAdapter
        }
    }

    private fun onClickEventoLista() {
        eventosAdapter
                .eventoClicado()
                .subscribe {
                    startActivity(DetalheDoEventoActivity.newIntent(context!!, it))
                }
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun onDestroy() {
        presenter.stop()
        super.onDestroy()
    }

    override fun showError(msg: String) {

    }
}