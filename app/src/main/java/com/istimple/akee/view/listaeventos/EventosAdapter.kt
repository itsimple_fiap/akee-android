package com.istimple.akee.view.listaeventos

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.istimple.akee.model.Evento
import com.itsimple.akee.R
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_evento.view.*
import kotlin.properties.Delegates

class EventosAdapter : RecyclerView.Adapter<EventoViewHolder>() {

    /**
     * A lista de eventos do Adapter, o objeto eventos ao ser atualizado atualiza automaticamente a listagem
     */

    var listaEventos: List<Evento> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    private val rowClick = PublishSubject.create<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            EventoViewHolder(
                    LayoutInflater
                            .from(parent.context)
                            .inflate(R.layout.item_evento, parent, false)
            )


    override fun getItemCount() = listaEventos.size

    fun eventoClicado(): Observable<String> = rowClick.hide()

    override fun onBindViewHolder(holder: EventoViewHolder, position: Int) {
        val evento = listaEventos[position]

        holder.itemView.setOnClickListener { rowClick.onNext(evento.id!!) }
        holder.bind(evento)
    }
}

/**
 * O ViewHolder do Adapter
 */
class EventoViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    /**
     * Vincula um evento a View
     */
    fun bind(evento: Evento) {
        view.textViewNomeEvento.text = evento.nome
        view.textViewDescricaoEvento.text = evento.descricao
        view.textViewPrecoEvento.text = if(evento.preco.toString() == "0.0") "GRÁTIS" else evento.preco.toString()
        Picasso.get()
                .load(evento.imagem)
                .resize(100,100)
                .centerCrop()
                .into(view.imageViewEvento)
    }
}