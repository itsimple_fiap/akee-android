package com.istimple.akee.view.perfil

import com.istimple.akee.model.User
import com.istimple.akee.repository.UserRepositoryContract
import com.istimple.akee.view.AbstractPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PerfilPresenter(override var view: PerfilContract.View,
                      private val repository: UserRepositoryContract) :
        AbstractPresenter<PerfilContract.View, PerfilContract.Presenter>(),
        PerfilContract.Presenter  {

    override lateinit var user: User

    override fun realizarAlteracao(user: User) {
        repository.realizarAlteracao(user)
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    view.showLoading()
                }.doOnComplete {
                    view.hideLoading()
                }.doOnError {
                    view.showError("")
                }.subscribe {
                    this.user = it
                    view.alteradoComSucesso()
                }
    }
    override fun realizarCadastro(user: User) {
        repository.realizarCadastro(user)
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    view.showLoading()
                }.doOnComplete {
                    view.hideLoading()
                }.doOnError {
                    view.showError("")
                }.subscribe {
                    view.cadastradoComSucesso()
                }
    }

}