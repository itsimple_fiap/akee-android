package com.istimple.akee.view.listaeventos

import com.istimple.akee.model.Evento
import com.istimple.akee.util.mvp.BasePresenter
import com.istimple.akee.util.mvp.BaseView

/**
 * Interface de métodos para visualizar Eventos
 */
interface EventosContract {

    interface View: BaseView<Presenter> {
        /**
         * Exibe o indicador de carregamento da tela
         */
        fun showLoading()

        /**
         * Oculta o indicador de carregamento da tela
         */
        fun hideLoading()

        /**
         * Atualiza as postagens anteriores pelas especificadas
         */
        fun updateEventos()

        fun showError(msg: String)
    }

    interface Presenter: BasePresenter<View> {
        var eventosList: List<Evento>

        fun getEventos()
    }
}