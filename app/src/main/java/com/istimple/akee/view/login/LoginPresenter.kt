package com.istimple.akee.view.login

import com.istimple.akee.repository.UserRepositoryContract
import com.istimple.akee.view.AbstractPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginPresenter(override var view: LoginContract.View,
                     private val repository: UserRepositoryContract) :
        AbstractPresenter<LoginContract.View, LoginContract.Presenter>(), LoginContract.Presenter {

    override fun realizarLogin(email: String, senha: String) {
        repository.realizarLogin(email, senha)
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    view.showLoading()
                }.doOnComplete {
                    view.hideLoading()
                }.doOnError {
                    view.showError("")
                }.subscribe {
                    view.loginComSucesso()
                }
    }
}