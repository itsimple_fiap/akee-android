package com.istimple.akee.view.smartCamera

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import com.google.api.services.vision.v1.model.Feature
import com.istimple.akee.util.mvp.BasePresenter
import com.istimple.akee.util.mvp.BaseView

interface SmartCameraContract {
    interface View: BaseView<Presenter> {
        fun takePictureFromCamera()
        fun startRequest()
        fun closeLoading()
        fun showError(error: String)
        fun requestSuccess(result: String)
    }

    interface Presenter: BasePresenter<View> {

        var visionAPI: List<String>
        var api: String

        fun callCloudVision(bitmap: Bitmap, feature: Feature)
        fun makeRequest(activity: Activity, permission: String)
        fun checkPermission(context: Context, permission: String): Int
    }
}