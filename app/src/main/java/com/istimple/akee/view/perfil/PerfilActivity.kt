package com.istimple.akee.view.perfil

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.istimple.akee.di.Parametros.PERFIL_VIEW
import com.istimple.akee.extensions.callGaleryActivity
import com.istimple.akee.extensions.isEmail
import com.istimple.akee.extensions.setRoundedImage
import com.istimple.akee.model.User
import com.istimple.akee.sharedPreferences.Prefs
import com.istimple.akee.util.LOGIN
import com.istimple.akee.view.login.LoginActivity
import com.istimple.akee.view.main.MainActivity
import com.itsimple.akee.R
import kotlinx.android.synthetic.main.activity_perfil.*
import org.koin.android.ext.android.inject

class PerfilActivity : AppCompatActivity(), PerfilContract.View {

    override val presenter: PerfilContract.Presenter by inject { mapOf(PERFIL_VIEW to this) }
    private val login by lazy { intent.getBooleanExtra(LOGIN, false) }
    private val RC_PHOTO_PICKER = 2
    private var uri: Uri? = null

    companion object {
        fun newIntent(ctx: Context, login: Boolean) = Intent(ctx, PerfilActivity::class.java)
                .putExtra(LOGIN, login)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)
        if (login) {
            title = getString(R.string.cadastrese)
            buttonCadastrarEditar.setText(R.string.cadastrese)
            buttonCadastrarEditar.tag = 0
        } else {
            title = getString(R.string.perfil)
            buttonCadastrarEditar.setText(R.string.editar)
            buttonCadastrarEditar.tag = 1
            imageViewSair.visibility = View.VISIBLE
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        buttonCadastrarEditar.setOnClickListener {
            clearErrorHint()

            if (validateFields()) {

                val user = User(
                        "",
                        editTextNome.text.toString(),
                        editTextEmail.text.toString(),
                        editTextSenha.text.toString(),
                        uri?.toString() ?: ""
                        )

                if (buttonCadastrarEditar.tag == 0) {
                    presenter.realizarCadastro(user)
                } else {
                    presenter.realizarAlteracao(user)
                }
            }
        }

        imageViewSair.setOnClickListener {
            Prefs(this).usuarioLogado = false
            startActivity(LoginActivity.newIntent(this))
            finish()
        }

        imageViewPerfil.setOnClickListener { callGaleryActivity(RC_PHOTO_PICKER) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_PHOTO_PICKER && resultCode == Activity.RESULT_OK) {
            val selectedImageUri = data?.data
            setPerfilImagem(selectedImageUri!!)
        }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun cadastradoComSucesso() {
        Toast.makeText(this, getString(R.string.cadastrado_com_sucesso), Toast.LENGTH_SHORT).show()
        Prefs(this).usuarioLogado = true
        startActivity(MainActivity.newIntent(this))
        finishAffinity()
    }

    override fun alteradoComSucesso() {
        preencherCampos()
        Toast.makeText(this, getString(R.string.alterado_com_sucesso), Toast.LENGTH_SHORT).show()
    }

    private fun preencherCampos() {
        editTextEmail.setText(presenter.user.email)
        editTextNome.setText(presenter.user.nome)
    }

    override fun showError(msg: String) {
        Toast.makeText(this, getString(R.string.erro), Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        presenter.stop()
        super.onDestroy()
    }

    private fun validateFields(): Boolean {

        return when {
            editTextNome.text.toString().isEmpty() -> {
                textInputLayoutNome.error = getString(R.string.nome_invalido)
                false
            }
            editTextEmail.text.toString().isEmpty()
                    || !editTextEmail.text.toString().isEmail() -> {

                textInputLayoutEmail.error = getString(R.string.email_invalido)
                false
            }
            editTextSenha.text.toString().isEmpty() -> {
                textInputLayoutSenha.error = getString(R.string.senha_invalida)
                false
            }
            else -> true
        }
    }

    private fun clearErrorHint() {
        textInputLayoutNome.error = null
        textInputLayoutEmail.error = null
        textInputLayoutSenha.error = null
    }

    private fun setPerfilImagem(uri: Uri) {
        imageViewPerfil.setRoundedImage(120, 120, uri)
        this.uri = uri
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
