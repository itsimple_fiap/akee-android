package com.istimple.akee.view.detalheevento

import com.istimple.akee.model.Evento
import com.istimple.akee.util.mvp.BasePresenter
import com.istimple.akee.util.mvp.BaseView

interface DetalheDoEventoContract {

    interface View: BaseView<Presenter> {
        fun showLoading()

        fun hideLoading()

        fun updateView()

        fun showError(msg: String)
    }

    interface Presenter: BasePresenter<View> {
        var evento: Evento

        fun getDetalheDoEvento(idEvento: String)
    }
}