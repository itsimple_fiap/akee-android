package com.istimple.akee.view.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.istimple.akee.sharedPreferences.Prefs
import com.istimple.akee.util.TAG_EVENTO_FAVORITOS_FRAGMENT
import com.istimple.akee.util.TAG_EVENTO_FRAGMENT
import com.istimple.akee.view.eventosfavoritos.FavoritosFragment
import com.istimple.akee.view.listaeventos.EventosFragment
import com.istimple.akee.view.login.LoginActivity
import com.istimple.akee.view.perfil.PerfilActivity
import com.istimple.akee.view.smartCamera.SmartCameraActivity
import com.itsimple.akee.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity: AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    var idNavigation = R.id.navigation_eventos
    companion object {
        fun newIntent(ctx: Context) = Intent(ctx, MainActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigationView.setOnNavigationItemSelectedListener(this)
        if (savedInstanceState == null) {
            navigationView.menu.performIdentifierAction(R.id.navigation_eventos, 0)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_eventos -> {
                title = getString(R.string.eventos)
                fragmentTransation(EventosFragment(), TAG_EVENTO_FRAGMENT)
                idNavigation = R.id.navigation_eventos
            }
            R.id.navigation_favoritos -> {
                title = getString(R.string.favoritos)
                fragmentTransation(FavoritosFragment(), TAG_EVENTO_FAVORITOS_FRAGMENT)
                idNavigation = R.id.navigation_favoritos
            }
            R.id.navigation_perfil -> {
                idNavigation = R.id.navigation_eventos
                if (Prefs(this).usuarioLogado) {
                    startActivity(PerfilActivity.newIntent(this, false))
                } else {
                    startActivity(LoginActivity.newIntent(this))
                }
            }
            R.id.navigation_camera -> {
                title = getString(R.string.smart_camera)
                startActivity(SmartCameraActivity.newIntent(this))
            }
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onPause() {
        super.onPause()
        navigationView.selectedItemId = idNavigation
    }

    private fun fragmentTransation(fragment: Fragment, tag: String) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, fragment, tag)
                .addToBackStack(tag)
                .commit()
    }
}