package com.istimple.akee

import android.app.Application
import com.istimple.akee.di.networkModule
import com.istimple.akee.di.presenterModule
import com.istimple.akee.di.repositoryModule
import com.itsimple.akee.BuildConfig
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class AkeeAplication: Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        setupKoin()

    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun setupKoin() {
        startKoin(arrayListOf(
                presenterModule,
                networkModule,
                repositoryModule))
    }
}