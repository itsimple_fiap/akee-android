package com.istimple.akee.di

import com.istimple.akee.di.Parametros.DETALHE_EVENTO_VIEW
import com.istimple.akee.di.Parametros.EVENTOS_VIEW
import com.istimple.akee.di.Parametros.FAVORITOS_VIEW
import com.istimple.akee.di.Parametros.LOGIN_VIEW
import com.istimple.akee.di.Parametros.PERFIL_VIEW
import com.istimple.akee.di.Parametros.SMART_CAMERA
import com.istimple.akee.view.detalheevento.DetalheDoEventoContract
import com.istimple.akee.view.detalheevento.DetalheDoEventoPresenter
import com.istimple.akee.view.eventosfavoritos.FavoritosContract
import com.istimple.akee.view.eventosfavoritos.FavoritosPresenter
import com.istimple.akee.view.listaeventos.EventosContract
import com.istimple.akee.view.listaeventos.EventosPresenter
import com.istimple.akee.view.login.LoginContract
import com.istimple.akee.view.login.LoginPresenter
import com.istimple.akee.view.perfil.PerfilContract
import com.istimple.akee.view.perfil.PerfilPresenter
import com.istimple.akee.view.smartCamera.SmartCameraContract
import com.istimple.akee.view.smartCamera.SmartCameraPresenter
import org.koin.dsl.module.applicationContext

object Parametros {
    const val EVENTOS_VIEW = "EVENTOS_VIEW"
    const val DETALHE_EVENTO_VIEW = "DETALHE_EVENTO_VIEW"
    const val FAVORITOS_VIEW = "FAVORITOS_VIEW"
    const val LOGIN_VIEW = "LOGIN_VIEW"
    const val PERFIL_VIEW = "PERFIL_VIEW"
    const val SMART_CAMERA = "SMART_CAMERA"
}

val presenterModule = applicationContext {

    factory { params -> EventosPresenter(params[EVENTOS_VIEW], get()) as EventosContract.Presenter }
    factory { params -> DetalheDoEventoPresenter(params[DETALHE_EVENTO_VIEW], get()) as DetalheDoEventoContract.Presenter }
    factory { params -> FavoritosPresenter(params[FAVORITOS_VIEW], get()) as FavoritosContract.Presenter }
    factory { params -> LoginPresenter(params[LOGIN_VIEW], get()) as LoginContract.Presenter }
    factory { params -> PerfilPresenter(params[PERFIL_VIEW], get()) as PerfilContract.Presenter }
    factory { params -> SmartCameraPresenter(params[SMART_CAMERA], get()) as SmartCameraContract.Presenter }
}