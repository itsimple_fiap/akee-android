package com.istimple.akee.di

import com.istimple.akee.repository.*
import org.koin.dsl.module.applicationContext

val repositoryModule = applicationContext {

    bean { EventosRepository(get()) as EventosRepositoryContract }
    bean { UserRepository(get()) as UserRepositoryContract }
    bean { CloudVisionRepository() as CloudVisionRepositoryContract }
}