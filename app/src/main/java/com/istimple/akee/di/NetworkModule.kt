package com.istimple.akee.di

import com.itsimple.akee.BuildConfig
import com.istimple.akee.network.EventoApi
import com.istimple.akee.util.BASE_URL
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

val networkModule = applicationContext {


    bean {
        HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
        } as Interceptor
    }

    bean {
        OkHttpClient.Builder()
                .addInterceptor(get())
                .build()
    }

    bean {
        Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BASE_URL)
                .client(get())
                .build()
    }

    bean {
        val retrofit: Retrofit = get()
        retrofit.create(EventoApi::class.java)
    }
}